import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Spotify demo',
      theme: ThemeData(fontFamily: 'Circular'),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget{
   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        centerTitle: false,
        title: Image.asset('assets/images/logo.png', height: 100, width: 100,), 
        ),
      endDrawer:Theme(
           data: Theme.of(context).copyWith(
                 canvasColor: Colors.black, ),
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.all(10),
              children: [
                SizedBox(height: 50,),
                ListTile(
                  title: const Text('Premium', style: TextStyle(fontSize: 40, color: Colors.white, fontWeight: FontWeight.w700)),
                  onTap: () { },
                ),
                SizedBox(height: 24,),
                ListTile(
                  title: const Text('Dukungan', style: TextStyle(fontSize: 40, color: Colors.white, fontWeight: FontWeight.w700)),
                  onTap: () { },
                ),
                SizedBox(height: 24,),
                ListTile(
                  title: const Text('Download', style: TextStyle(fontSize: 40, color: Colors.white, fontWeight: FontWeight.w700)),
                  onTap: () { },
                ),
              Text('  ——', style: TextStyle(fontSize: 40, color: Colors.white, fontWeight: FontWeight.w500)),
              SizedBox(height: 24,),
              ListTile(
                  title: const Text('Daftar', style: TextStyle(fontSize: 30, color: Colors.white, fontWeight: FontWeight.w500)),
                  onTap: () { },
                ),
                SizedBox(height: 15,),
                ListTile(
                  title: const Text('Masuk', style: TextStyle(fontSize: 30, color: Colors.white, fontWeight: FontWeight.w500)),
                  onTap: () { },
                ),
              ],
            ),
          )
  ),
  body: SingleChildScrollView(
    child:  Center(
    child: Column(
        children: [ 
          top(),
        ]
    )
  )
  )
    );
}
}

class top extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Column(
      children: [
        Container(
          height: 600,
          decoration: BoxDecoration(
         color: Color(0xFF2D46B9),
         image: DecorationImage(
                image: AssetImage("assets/images/background.png"),
          fit: BoxFit.fill
              )
                 ),
          child: Column(children: [
          SizedBox(height: 180,),
           Center(
           child: Text(
             'Listening is Everything', textAlign: TextAlign.center, style: TextStyle(fontSize: 50, fontWeight: FontWeight.w800, color: Color(0XFF1ED760)), 
             )),
          Padding(padding: EdgeInsets.all(16.0),
          child:ElevatedButton(// Respond to button press
              onPressed: () { },
              style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Color(0XFF1ED760),),shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                )
                
              )),
              child: Text(
                "DAPATKAN SPOTIFY FREE", 
                style: TextStyle(color: Color(0xFF2D46B9),
                    fontSize: 20, fontWeight: FontWeight.w500),
              ),
            )),
          ],)
              ),
      ]
      );
           
  }
}
