Apakah perbedaan antara JSON dan XML?
Apakah perbedaan antara HTML dan XML?

1.  -From the acronym itself, the difference between JSON and XML is obvious.
    JSON stands for JavaScript Object Notation, meanwhile
    XML stands for Extensible Markup Language. By looking at both of these elaborations, we know that XML is a form of markup language and XML utilize tag to represent the data. Whereas JSON only represents objects.
    -JSON is created from the JavaScript language, on the other hand, XML is based on Standard Generalized Markup Language
    -JSON files are more comprehensive for the human eyes, unlike XML whose files are difficult to understand.
    -In terms of security, XML is more secured in comparison to JSON
    -JSON allows the utilization of array, but not comments. XML is the opposite, it doesn't support the usage of array but it allows comments.

2.  -HTML stands for Hyper Text Markup Languange. The purpose of using HTML is to 
    construct web page or the display of a web application. Unlike HTML, XML is used to build the structure, to save, and to allocate the information. In addition, XML is also utilized to describe the data.
    -Different from XML, 
    HTML is static, meanwhile XML is dynamic. This means that the web display in HTML will stay the same, hence the name static. Whereas in XML, the information may shift, which then will create changes on the display. 
    -Judging from both of their characteristics, there are also small differences that HTML and XML share. In HTML, tiny errors are often ignore and does not have that much of an effect, unlike XML who is extremely sensitive to errors. Besides from that, HTML is not case sensitive in terms of typewriting. Meanwhile XML does. 
    -Based on the main function, HTML is task assign to merely display the data that is saved in the XML.

