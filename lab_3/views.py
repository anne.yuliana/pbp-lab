from django.shortcuts import render
from lab_1.models import Friend
from django.http.response import HttpResponseRedirect
from .forms import FriendForm
from django.contrib.auth.decorators import login_required
# Create your views here.
@login_required(login_url = "/admin/login/?next")
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    #ngambil semua objek yg terdaftar sbg class friend
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        #check the validity of the form
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3/')
    
    else:
        form = FriendForm()

    return render(request, 'lab3_form.html', {'form': form})